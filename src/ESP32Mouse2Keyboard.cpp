/*
 * This program is based on https://github.com/h2zero/NimBLE-Arduino/tree/master/examples/NimBLE_Client.
 * My changes are covered by the MIT license.
 */

/*
 * MIT License
 *
 * Copyright (c) 2023 touchgadgetdev@gmail.com
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <Arduino.h>
//#include "M5AtomS3.h"
#include "M5Unified.h"
#include <SD.h>
#include "keyboard_classifier.h"

#define DUMP_REPORT_MAP 0
#define DEV_INFO_SERVICE 0  // BLE device information

//#define ARDUINO_USB_MODE 0  
#define EMULATE_KEYBOARD 1  // 1 to production, 0 for ebug (ekeyboar emulation blocks usb debug)
#define USB_DEBUG 0 // 01 to get USB message for debugging , 0 for production
//#define ARDUINO_USB_CDC_ON_BOOT 1

#define ARDUINO_M5Stack_ATOMS3  // define for specific AtomS3 code

#if EMULATE_KEYBOARD == 1
#include "hidkeyboard.h"
#endif

// Set to 0 to remove the CDC ACM serial port but XAC seems to tolerate it
// so it is on by default. Set to 0 if it causes problems. Disabling the
// CDC port means you must press button(s) on the ESP32S3 to put in bootloader
// upload mode before using the IDE to upload.
#if USB_DEBUG
#define DBG_begin(...) Serial.begin(__VA_ARGS__)
#define DBG_end(...) Serial.end(__VA_ARGS__)
#define DBG_print(...) Serial.print(__VA_ARGS__)
#define DBG_println(...) Serial.println(__VA_ARGS__)
#define DBG_printf(...) Serial.printf(__VA_ARGS__)
#define DBG_flush() Serial.flush()
#else
#define DBG_begin(...)
#define DBG_end(...)
#define DBG_print(...)
#define DBG_println(...)
#define DBG_printf(...)
#define DBG_flush()

#endif



//#include <OneButton.h>  // https://github.com/mathertel/OneButton
//#include <FastLED.h>    // https://github.com/FastLED/FastLED
//#include <M5GFX.h>      // https://github.com/m5stack/M5GFX
/* M5Stack AtomS3 display, RGB LED, button */
//M5GFX display;
#define TFT_print(...) \
  do { \
    clrscr(); \
    M5.Display.print(__VA_ARGS__); \
  } while (0)
#define TFT_println(...)             \
  do                                 \
  {                                  \
    clrscr();                        \
    M5.Display.println(__VA_ARGS__); \
  } while (0)
#define TFT_printf(...)             \
  do                                \
  {                                 \
    clrscr();                       \
    M5.Display.printf(__VA_ARGS__); \
  } while (0)
#define TFT_color(foreground, background) display.setTextColor(foreground, background)
#define RGBLed(color) \
  do { \
    RGBled = color; \
    FastLED.show(); \
  } while (0)

//static const int LED_DI_PIN = 35;
//CRGB RGBled;

static const int BTN_PIN = 41;
//OneButton button(BTN_PIN, true);

static const int TEXT_SIZE = 2;

typedef struct {
  uint8_t report[32];
  size_t report_len;
  uint32_t last_millis = 0;

  int16_t xmin = SCHAR_MIN;
  int16_t xmax = SCHAR_MAX;
  int16_t ymin = SCHAR_MIN;
  int16_t ymax = SCHAR_MAX;
  bool isNotify;
  bool available;
  bool isJellyComb;
} Mouse_xfer_state_t;

volatile Mouse_xfer_state_t Mouse_xfer;

extern "C" {
#include "./report_desc.h"
}

const uint16_t JellyComb_VID = 0x1915;
const uint16_t JellyComb_PID = 0x0040;
const uint16_t VRFortune_VID = 0x07d7;
const uint16_t VRFortune_PID = 0x0000;

// Install NimBLE-Arduino by h2zero using the IDE library manager.
#include <NimBLEDevice.h>

const uint16_t APPEARANCE_HID_GENERIC = 0x3C0;
const uint16_t APPEARANCE_HID_KEYBOARD = 0x3C1;
const uint16_t APPEARANCE_HID_MOUSE = 0x3C2;
const uint16_t APPEARANCE_HID_JOYSTICK = 0x3C3;
const uint16_t APPEARANCE_HID_GAMEPAD = 0x3C4;
const uint16_t APPEARANCE_HID_DIGITIZER_TABLET = 0x3C5;
const uint16_t APPEARANCE_HID_CARD_READER = 0x3C6;
const uint16_t APPEARANCE_HID_DIGITAL_PEN = 0x3C7;
const uint16_t APPEARANCE_HID_BARCODE_SCANNER = 0x3C8;
const uint16_t APPEARANCE_HID_TOUCHPAD = 0x3C9;
const uint16_t APPEARANCE_HID_PRESENTATION_REMOTE = 0x3CA;

const char DEVICE_INFORMATION_SERVICE[] = "180A";
const char DIS_SYSTEM_ID_CHAR[] = "2A23";
const char DIS_MODEL_NUM_CHAR[] = "2A24";
const char DIS_SERIAL_NUM_CHAR[] = "2A25";
const char DIS_FIRMWARE_REV_CHAR[] = "2A26";
const char DIS_HARDWARE_REV_CHAR[] = "2A27";
const char DIS_SOFTWARE_REV_CHAR[] = "2A28";
const char DIS_MANUFACTURE_NAME_CHAR[] = "2A29";
const char DIS_PNP_ID_CHAR[] = "2A50";
const char HID_SERVICE[] = "1812";
const char HID_INFORMATION[] = "2A4A";
const char HID_REPORT_MAP[] = "2A4B";
const char HID_CONTROL_POINT[] = "2A4C";
const char HID_REPORT_DATA[] = "2A4D";
const char HID_PROTOCOL_MODE[] = "2A4E";
const char HID_BOOT_KEYBOARD_OUTPUT_REPORT[] = "2A32";
const char HID_BOOT_MOUSE_INPUT_REPORT[] = "2A33";

void scanEndedCB(NimBLEScanResults results);

static NimBLEAdvertisedDevice* advDevice;

static bool doConnect = false;
static uint32_t scanTime = 0; /** 0 = scan forever */

char buffer[128];  // used for sprintf

#if EMULATE_KEYBOARD == 1
HIDkeyboard keyboardDevice;
#endif
// ===== various  functions =================================================================

void displayMsg(const char* msg) {
  M5.Display.clear();
  M5.Display.drawString(msg, M5.Display.width() / 2, M5.Display.height() / 2);
  //TFT_color(TFT_GREEN, TFT_BLACK);
  //TFT_print(msg);
  DBG_println(msg);
}

/**  None of these are required as they will be handled by the library with defaults. **
 **                       Remove as you see fit for your needs                        */
class ClientCallbacks : public NimBLEClientCallbacks {
  void onConnect(NimBLEClient* pClient) {
    DBG_println("Connected");
    //TFT_println("Connected");
    /** After connection we should change the parameters if we don't need fast response times.
     *  These settings are 150ms interval, 0 latency, 450ms timout.
     *  Timeout should be a multiple of the interval, minimum is 100ms.
     *  I find a multiple of 3-5 * the interval works best for quick response/reconnect.
     *  Min interval: 120 * 1.25ms = 150, Max interval: 120 * 1.25ms = 150, 0 latency, 60 * 10ms = 600ms timeout
     */
    pClient->updateConnParams(120, 120, 0, 60);
    DBG_printf("%s: peer MTU %u\n", __func__, pClient->getMTU());
  };

  void onDisconnect(NimBLEClient* pClient) {
    DBG_print(pClient->getPeerAddress().toString().c_str());
    displayMsg("Disconnect-Scanning");
    //RGBLed(CRGB::Yellow);
    NimBLEDevice::getScan()->start(scanTime, scanEndedCB);
  };

  /** Called when the peripheral requests a change to the connection parameters.
   *  Return true to accept and apply them or false to reject and keep
   *  the currently used parameters. Default will return true.
   */
  bool onConnParamsUpdateRequest(NimBLEClient* pClient, const ble_gap_upd_params* params) {
    // Failing to accepts parameters may result in the remote device
    // disconnecting.
    return true;
  };

  /********************* Security handled here **********************
   ****** Note: these are the same return values as defaults ********/
  uint32_t onPassKeyRequest() {
    DBG_println("Client Passkey Request");
    /** return the passkey to send to the server */
    return 123456;
  };

  bool onConfirmPIN(uint32_t pass_key) {
    DBG_print("The passkey YES/NO number: ");
    DBG_println(pass_key);
    /** Return false if passkeys don't match. */
    return true;
  };

  /** Pairing process complete, we can check the results in ble_gap_conn_desc */
  void onAuthenticationComplete(ble_gap_conn_desc* desc) {
    if (!desc->sec_state.encrypted) {
      DBG_println("Encrypt connection failed - disconnecting");
      /** Find the client with the connection handle provided in desc */
      NimBLEDevice::getClientByID(desc->conn_handle)->disconnect();
      return;
    }
  };
};

/** Define a class to handle the callbacks when advertisments are received */
class AdvertisedDeviceCallbacks : public NimBLEAdvertisedDeviceCallbacks {

  void onResult(NimBLEAdvertisedDevice* advertisedDevice) {
    uint8_t advType = advertisedDevice->getAdvType();
    if ((advType == BLE_HCI_ADV_TYPE_ADV_DIRECT_IND_HD) || (advType == BLE_HCI_ADV_TYPE_ADV_DIRECT_IND_LD) || (advertisedDevice->haveServiceUUID() && advertisedDevice->isAdvertisingService(NimBLEUUID(HID_SERVICE)))) {
      DBG_printf("onResult: AdvType= %d\r\n", advType);
      DBG_print("Advertised HID Device found: ");
      DBG_println(advertisedDevice->toString().c_str());

      /** stop scan before connecting */
      NimBLEDevice::getScan()->stop();
      /** Save the device reference in a global for the client to use*/
      advDevice = advertisedDevice;
      /** Ready to connect now */
      doConnect = true;
    }
  };
};

/** Notification / Indication receiving handler callback */
// Notification from 4c:75:25:xx:yy:zz: Service = 0x1812, Characteristic = 0x2a4d, Value = 1,0,0,0,0,
void notifyCB(NimBLERemoteCharacteristic* pRemoteCharacteristic,
              uint8_t* pData, size_t length, bool isNotify) {
  if (Mouse_xfer.available) {
    static uint32_t dropped = 0;
    DBG_printf("drops=%u\r\n", ++dropped);
  } else {
    DBG_println(pRemoteCharacteristic->toString().c_str());
    Mouse_xfer.isNotify = isNotify;
    Mouse_xfer.report_len = length;
    memcpy((void*)Mouse_xfer.report, pData, min(length, sizeof(Mouse_xfer.report)));
    Mouse_xfer.available = true;
    Mouse_xfer.last_millis = millis();
  }
}

/** Callback to process the results of the last scan or restart it */
void scanEndedCB(NimBLEScanResults results) {
  DBG_println("Scan Ended");
}

/** Create a single global instance of the callback class to be used by all clients */
static ClientCallbacks clientCB;

static NimBLEAddress LastBLEAddress;

/** Handles the provisioning of clients and connects / interfaces with the server */
bool connectToServer() {
  NimBLEClient* pClient = nullptr;
  bool reconnected = false;

  DBG_printf("Client List Size: %d\r\n", NimBLEDevice::getClientListSize());
  /** Check if we have a client we should reuse first **/
  if (NimBLEDevice::getClientListSize()) {
    /** Special case when we already know this device, we send false as the
     *  second argument in connect() to prevent refreshing the service database.
     *  This saves considerable time and power.
     */
    pClient = NimBLEDevice::getClientByPeerAddress(advDevice->getAddress());
    if (pClient) {
      if (pClient->getPeerAddress() == LastBLEAddress) {
        if (!pClient->connect(advDevice, false)) {
          DBG_println("Reconnect failed");
          displayMsg("Reconnect failed");
          return false;
        }
        DBG_println("Reconnected client");
        displayMsg("Reconnected client");
        reconnected = true;
      }
    }
    /** We don't already have a client that knows this device,
     *  we will check for a client that is disconnected that we can use.
     */
    else {
      pClient = NimBLEDevice::getDisconnectedClient();
    }
  }

  /** No client to reuse? Create a new one. */
  if (!pClient) {
    if (NimBLEDevice::getClientListSize() >= NIMBLE_MAX_CONNECTIONS) {
      DBG_println("Max clients reached - no more connections available");
      return false;
    }

    pClient = NimBLEDevice::createClient();

    DBG_println("New client created");

    pClient->setClientCallbacks(&clientCB, false);
    /** Set initial connection parameters: These settings are 15ms interval, 0 latency, 120ms timout.
     *  These settings are safe for 3 clients to connect reliably, can go faster if you have less
     *  connections. Timeout should be a multiple of the interval, minimum is 100ms.
     *  Min interval: 12 * 1.25ms = 15, Max interval: 12 * 1.25ms = 15, 0 latency, 51 * 10ms = 510ms timeout
     */
    pClient->setConnectionParams(12, 12, 0, 51);
    /** Set how long we are willing to wait for the connection to complete (seconds), default is 30. */
    pClient->setConnectTimeout(5);


    if (!pClient->connect(advDevice)) {
      /** Created a client but failed to connect, don't need to keep it as it has no data */
      NimBLEDevice::deleteClient(pClient);
      DBG_println("Failed to connect, deleted client");
      return false;
    }
  }

  if (!pClient->isConnected()) {
    if (!pClient->connect(advDevice)) {
      DBG_println("Failed to connect");
      return false;
    }
  }

  LastBLEAddress = pClient->getPeerAddress();
  DBG_print("Connected to: ");
  DBG_println(pClient->getPeerAddress().toString().c_str());
  DBG_print("RSSI: ");
  DBG_println(pClient->getRssi());

  /** Now we can read/write/subscribe the charateristics of the services we are interested in */
  NimBLERemoteService* pSvc = nullptr;
  NimBLERemoteCharacteristic* pChr = nullptr;
  NimBLERemoteDescriptor* pDsc = nullptr;

  Mouse_xfer.xmin = SCHAR_MIN;
  Mouse_xfer.xmax = SCHAR_MAX;
  Mouse_xfer.ymin = SCHAR_MIN;
  Mouse_xfer.ymax = SCHAR_MAX;

#if DEV_INFO_SERVICE
  // Device Information Service
  pSvc = pClient->getService(DEVICE_INFORMATION_SERVICE);
  if (pSvc) { /** make sure it's not null */
    DBG_println(pSvc->toString().c_str());
    std::vector<NimBLERemoteCharacteristic*>* charvector;
    charvector = pSvc->getCharacteristics(true);
    for (auto& it : *charvector) {
      DBG_print(it->toString().c_str());
      if (it->canRead()) {
        DBG_print(" Value: ");
        DBG_println(it->readValue().c_str());
      }
    }
    pChr = pSvc->getCharacteristic(DIS_PNP_ID_CHAR);
    if (pChr) { /** make sure it's not null */
      if (pChr->canRead()) {
        typedef struct __attribute__((__packed__)) {
          uint8_t vendor_id_source;
          uint16_t vendor_id;
          uint16_t product_id;
          uint16_t product_version;
        } pnp_id_t;
        pnp_id_t* pnp_id = (pnp_id_t*)pChr->readValue().data();
        uint16_t vid = pnp_id->vendor_id;
        uint16_t pid = pnp_id->product_id;
        DBG_printf("PNP ID: source: %02x, vendor: %04x, product: %04x, version: %04x\r\n",
                   pnp_id->vendor_id_source, vid, pid, pnp_id->product_version);
        Mouse_xfer.isJellyComb =
          ((vid == JellyComb_VID) && (pid == JellyComb_PID));
      }
    }
  } else {
    DBG_println("Device Information Service not found.");
  }
#endif

  pSvc = pClient->getService(HID_SERVICE);
  if (pSvc) { /** make sure it's not null */
    // This returns the HID report descriptor like this
    // HID_REPORT_MAP 0x2a4b Value: 5,1,9,2,A1,1,9,1,A1,0,5,9,19,1,29,5,15,0,25,1,75,1,
    // Copy and paste the value digits to http://eleccelerator.com/usbdescreqparser/
    // to see the decoded report descriptor.
    pChr = pSvc->getCharacteristic(HID_REPORT_MAP);
    if (pChr) { /** make sure it's not null */
      if (pChr->canRead()) {
        std::string value = pChr->readValue();
        if (!reconnected) {
          parse_hid_report_descriptor((const uint8_t*)value.c_str(),
                                      (size_t)value.length(), false);
        }
#if DUMP_REPORT_MAP
        DBG_println("HID_REPORT_MAP ");
        DBG_print(pChr->getUUID().toString().c_str());
        DBG_print(" Value: ");
        uint8_t* p = (uint8_t*)value.data();
        for (size_t i = 0; i < value.length(); i++) {
          DBG_print(p[i], HEX);
          DBG_print(',');
        }
        DBG_println();
#endif
      }
    } else {
      DBG_println("HID REPORT MAP char not found.");
    }

    // Subscribe to characteristics HID_REPORT_DATA.
    // One real device reports 2 with the same UUID but
    // different handles. Using getCharacteristic() results
    // in subscribing to only one.
    std::vector<NimBLERemoteCharacteristic*>* charvector;
    charvector = pSvc->getCharacteristics(true);
    for (auto& it : *charvector) {
      if (it->getUUID() == NimBLEUUID(HID_REPORT_DATA)) {
        DBG_println(it->toString().c_str());
        if (it->canNotify()) {
          if (it->subscribe(true, notifyCB)) {
            displayMsg("subscribe notification OK");
          } else {
            /** Disconnect if subscribe failed */
            displayMsg("subscribe notification failed");
            pClient->disconnect();
            return false;
          }
        }
        if (it->canIndicate()) {
          if (it->subscribe(false, notifyCB)) {
            displayMsg("subscribe indication OK");
          } else {
            /** Disconnect if subscribe failed */
            displayMsg("subscribe indication failed");
            pClient->disconnect();
            return false;
          }
        }
      }
    }
  }
  DBG_println("Done with this device!");
  return true;
}

// === DIRECTION ARROW DEFINITIONS AND METHODS ===============================================

#define NO_DIRECTION 0
#define DIRECTION_UP 1
#define DIRECTION_DOWN 2
#define DIRECTION_LEFT 3
#define DIRECTION_RIGHT 4

/* defined in tinyusb/src/class/hid.hid.h
#define HID_KEY_ARROW_RIGHT               0x4F
#define HID_KEY_ARROW_LEFT                0x50
#define HID_KEY_ARROW_DOWN                0x51
#define HID_KEY_ARROW_UP                  0x52
*/

//static inline int abs (int x ) { return (x > 0) ? x : -x ;}

int mouse_treshold = 5;  // moremove small movements
int repeat_delay = 300;  // delay before beginning to repear
int repeat_rate = 200;   // delay when repeating

const char *direction_to_string(int direction)
{
  switch (direction)
  {
  case NO_DIRECTION:
    return ("0");
  case DIRECTION_RIGHT:
    return ("RIGHT");
  case DIRECTION_LEFT:
    return ("LEFT");
  case DIRECTION_UP:
    return ("UP");
  case DIRECTION_DOWN:
    return ("DOWN");
  default:
    return ("???");
  }
  return ("===");
}

int classifyMouseXY(int x, int y)
{
  if (abs(x) > abs(y))
  {
    // X AXIS movement
    if (abs(x) < mouse_treshold)
    {
      return (NO_DIRECTION);
    }
    else if (x > 0)
    {
      return (DIRECTION_RIGHT);
    }
    else
    {
      return (DIRECTION_LEFT);
    }
  }
  else
  {
    // Y-AXIS MOVEMENT
    if (abs(y) < mouse_treshold)
    {
      return (NO_DIRECTION);
    }
    else if (y > 0)
    {
      return (DIRECTION_DOWN);
    }
    else
    {
      return (DIRECTION_UP);
    }
  }
}

void sendDirectionKey(int direction) {
  displayMsg(direction_to_string(direction));
#if EMULATE_KEYBOARD == 1
  int hidkey = 0;
  if (direction == NO_DIRECTION) {
    return;
  }
  if (direction == DIRECTION_UP) {
    hidkey = HID_KEY_ARROW_UP;
  } else if (direction == DIRECTION_DOWN) {
    hidkey = HID_KEY_ARROW_DOWN;
  } else if (direction == DIRECTION_LEFT) {
    hidkey = HID_KEY_ARROW_LEFT;
  } else if (direction == DIRECTION_RIGHT) {
    hidkey = HID_KEY_ARROW_RIGHT;
  }
  keyboardDevice.sendKey(hidkey);
#endif
  return;
}

static void test(int testSequence[], int sequenceLen, int time_step) {
  KeyboardStateAutomata automate;
  int t = 0;
  for (int i = 0; i < sequenceLen; i++) {
    automate.doAction(testSequence[i], t);
    t += time_step;
  }
}

void testkeyboardAutomata() {
  int test1[15] = { 0, 1, 3, 3, 3, 2, 4, 4, 4, 4, 4, 4, 4, 4, 0 };
  int test2[20] = { 0, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 3 };  // test auto repear and delay
  test(test1, 15, 100);
  test(test2, 20, 100);
}

KeyboardStateAutomata automate;

// ====== SETUP =================================================================
void setup() {

  auto cfg = M5.config();
  cfg.serial_baudrate = 115200;
  M5.begin(cfg);
  //DBG_begin(115200);

  M5.Display.setTextColor(GREEN);
  M5.Display.setTextDatum(middle_center);
  M5.Display.setTextFont(&fonts::Orbitron_Light_24);
  M5.Display.setTextSize(1);
  M5.Display.drawString("Prêt!", M5.Display.width() / 2,
                        M5.Display.height() / 2);

#if USB_DEBUG == 1
  while (!Serial) delay(10);
    //while (!Serial && (millis() < 3000)) delay(10);  // wait for native usb
#endif
  DBG_println("Test");
  DBG_flush();
  // esp_wifi_stop();

  M5.Display.drawString("Click!", M5.Display.width() / 2,
                        M5.Display.height() / 2);
  DBG_println("Click BtnA to Test");
  DBG_flush();

  //setup_m5stack_atoms3();

  automate.repeatDelay = 600;  // repeat delay (like in windows 11) typica lly between 250ms and 1000ms
  automate.repeatRate = 300;
  automate.send_direction_callback = &sendDirectionKey ;
  //testkeyboardAutomata() ;
#if EMULATE_KEYBOARD == 1
                                         keyboardDevice.begin();
#endif


#if defined(ARDUINO_M5Stack_ATOMS3)
  // Place holder code for various button inputs. Replace or delete the code
  // as needed.
#if 0
  button.attachClick([] {
      displayMsg("Button click");
      });
  button.attachDoubleClick([] {
      displayMsg("Button double click");
      });
#endif
/*
    button.attachMultiClick([] {
    //reset settings - wipe bonding credentials
    NimBLEDevice::deleteAllBonds();
    displayMsg("Bonds Erased");
    delay(1000);
    ESP.restart();
  });
  */
#if 0
  button.attachLongPressStart([] {
      displayMsg("Button long press start");
      });
  button.attachDuringLongPress([] {
      displayMsg("Button during long press");
      });
  button.attachLongPressStop([] {
      displayMsg("Button long press stop");
      });
#endif

#endif  // defined(ARDUINO_LILYGO_T_DISPLAY_S3) || defined(ARDUINO_M5Stack_ATOMS3)

  DBG_println("Starting NimBLE HID Client");
  /** Initialize NimBLE, no device name spcified as we are not advertising */
  NimBLEDevice::init("");
  if (NimBLEDevice::setMTU(512)) {
    DBG_printf("%s: setMTU(512) failed\n", __func__);
  }
  DBG_printf("%s: getMTU %d\n", __func__, NimBLEDevice::getMTU());

  /** Set the IO capabilities of the device, each option will trigger a different pairing method.
   *  BLE_HS_IO_KEYBOARD_ONLY    - Passkey pairing
   *  BLE_HS_IO_DISPLAY_YESNO   - Numeric comparison pairing
   *  BLE_HS_IO_NO_INPUT_OUTPUT - DEFAULT setting - just works pairing
   */
  //NimBLEDevice::setSecurityIOCap(BLE_HS_IO_KEYBOARD_ONLY); // use passkey
  //NimBLEDevice::setSecurityIOCap(BLE_HS_IO_DISPLAY_YESNO); //use numeric comparison

  /** 2 different ways to set security - both calls achieve the same result.
   *  no bonding, no man in the middle protection, secure connections.
   *
   *  These are the default values, only shown here for demonstration.
   */
  NimBLEDevice::setSecurityAuth(true, true, true);
  //NimBLEDevice::setSecurityAuth(/*BLE_SM_PAIR_AUTHREQ_BOND | BLE_SM_PAIR_AUTHREQ_MITM |*/ BLE_SM_PAIR_AUTHREQ_SC);

  /** Optional: set the transmit power, default is 3db */
  NimBLEDevice::setPower(ESP_PWR_LVL_P9); /** +9db */

  /** Optional: set any devices you don't want to get advertisments from */
  // NimBLEDevice::addIgnored(NimBLEAddress ("aa:bb:cc:dd:ee:ff"));

  /** create new scan */
  NimBLEScan* pScan = NimBLEDevice::getScan();

  /** create a callback that gets called when advertisers are found */
  pScan->setAdvertisedDeviceCallbacks(new AdvertisedDeviceCallbacks());

  /** Set scan interval (how often) and window (how long) in milliseconds */
  pScan->setInterval(22);
  pScan->setWindow(11);

  /** Active scan will gather scan response data from advertisers
   *  but will use more energy from both devices
   */
  pScan->setActiveScan(false);
  /** Start scanning for advertisers for the scan time specified (in seconds) 0 = forever
   *  Optional callback for when scanning stops.
   */
  displayMsg("Scanning");
  pScan->start(scanTime, scanEndedCB);
}

static inline int smin(int x, int y) {
  return (x < y) ? x : y;
}
static inline int smax(int x, int y) {
  return (x > y) ? x : y;
}

// ====== LOOP ===========================================================================
void loop() {
  /*
  M5.update();
  if (M5.BtnA.wasPressed()) {
    displayMsg ("PRESSED");
    DBG_flush();
  }
  if (M5.BtnA.wasReleased()) {
    displayMsg("Released");
  }
  */
  /** Loop here until we find a device we want to connect to */
  if (doConnect) {
    //TFT_println(advDevice->toString().c_str());
    sprintf(buffer, advDevice->toString().c_str());
    displayMsg(buffer);
    doConnect = false;

    /** Found a device we want to connect to, do it now */
    if (connectToServer()) {
      displayMsg("Success! we should now be getting notifications!");
      //TFT_color(TFT_GREEN, TFT_BLACK);
      displayMsg("Mouse to XAC");
      //RGBLed(CRGB::Green);
    } else {
      displayMsg("Failed to connect, starting scan");
      //TFT_color(TFT_YELLOW, TFT_BLACK);
      displayMsg("Connect fail\nScanning");
      //RGBLed(CRGB::Yellow);
      NimBLEDevice::getScan()->start(scanTime, scanEndedCB);
    }
  } else if (Mouse_xfer.available) {
#if 0
    DBG_printf("%s:", __func__);
    for (size_t i = 0; i < Mouse_xfer.report_len; i++) {
      DBG_printf(" %02x", Mouse_xfer.report[i]);
    }
    DBG_println();
#endif
    mouse_values_t ble_mouse;
    extract_mouse_values((const uint8_t*)Mouse_xfer.report, &ble_mouse);
    Mouse_xfer.available = false;
    // DBG_printf("id %d buttons %x, x %d, y %d\n", ble_mouse.report_id,
    //    ble_mouse.buttons, ble_mouse.x, ble_mouse.y);

    Mouse_xfer.xmin = smin(ble_mouse.x, Mouse_xfer.xmin);
    Mouse_xfer.xmax = smax(ble_mouse.x, Mouse_xfer.xmax);
    Mouse_xfer.ymin = smin(ble_mouse.y, Mouse_xfer.ymin);
    Mouse_xfer.ymax = smax(ble_mouse.y, Mouse_xfer.ymax);
    /*
    Mouse_xfer.joyRpt.x = map(ble_mouse.x,
                              Mouse_xfer.xmin, Mouse_xfer.xmax, 0, 1023);
    Mouse_xfer.joyRpt.y = map(ble_mouse.y,
                              Mouse_xfer.ymin, Mouse_xfer.ymax, 0, 1023);
                              */

    int xx = ble_mouse.x;
    int yy = ble_mouse.y;

    sprintf(buffer, "x= %d, y= %d", xx, yy);
    DBG_println(buffer);
    int direction = classifyMouseXY((int)ble_mouse.x, (int)ble_mouse.y);
    automate.doAction(direction, millis());
    //displayMsg (direction_to_string(direction)) ;


  } else {
    if ((millis() - Mouse_xfer.last_millis) > 31) {
      // Center x,y if no HID report for 32 ms. Preserve the buttons.
      //Mouse_xfer.joyRpt.x = 511;
      //Mouse_xfer.joyRpt.y = 511;
      //FSJoy.write((void*)&Mouse_xfer.joyRpt, sizeof(Mouse_xfer.joyRpt));
      //TFT_print('center');
      Mouse_xfer.last_millis = millis();
    }
  }
}
