
// === DIRECTION ARRAOW DEFINITIONS AND METHODS ===============================================

#include <stdlib.h>

#include "config_local.h"
#include "keyboard_classifier.h"

#define displayMsg(MESSAGE) \
    {                       \
        ;                   \
    }
#define DBG_println(MESSAGE) \
    {                        \
        ;                    \
    }
#define DBG_print(MESSAGE) \
    {                      \
        ;                  \
    }
// static inline int abs (int x ) { return (x > 0) ? x : -x ;}

// ==== KEYBOARD AUTOMATA


KeyboardStateAutomata::KeyboardStateAutomata()
{
    // init
    state = S_0;
    send_direction_callback = NULL ;
}

void KeyboardStateAutomata::doAction(int direction, unsigned long time)
{
    State next = nextState(direction, time);
    // char buffer[256];
    // sprintf(buffer, "toto");
    DBG_print("time: ");
    DBG_print(time);
    DBG_print(" :: current_state: ");
    DBG_print(state);
    DBG_print(", direction: ");
    DBG_print(direction);
    DBG_print(", next_state: ");
    DBG_print(next);
    DBG_println();

    enterState(next);
}

State KeyboardStateAutomata::nextState(int direction, unsigned long time)
{
    State nextState = S_0;
    if (state == S_0)
    { // === S_0 ====================
        time_begin_delay = 0;
        time_begin_rate = 0;
        if (direction == 0)
            nextState = S_0;
        else
        {
            nextState = S_RepeatDelayEnter;
        }
    }
    if (state == S_RepeatDelayEnter)
    { // === S_RepeatDelayEnter ====================
        if (direction != prev_direction)
        {
            nextState = S_0;
        }
        else if (direction != prev_direction)
        {
            nextState = S_RepeatDelayEnter;
        }
        else if ((direction == prev_direction))
        {
            time_begin_delay = time;
            nextState = S_RepeatDelayWait;
        }
    }
    if (state == S_RepeatDelayWait)
    { // === S_RepeatDelayWait ====================
        if (direction == 0)
        {
            nextState = S_0;
        }
        else if (direction != prev_direction)
        {
            nextState = S_RepeatDelayEnter;
        }
        else if ((direction == prev_direction) && (time < (time_begin_delay + repeatDelay)))
        {
            nextState = S_RepeatDelayWait;
        }
        else if ((direction == prev_direction) && (time >= (time_begin_delay + repeatDelay)))
        {
            nextState = S_RepeatRateEnter;
        }
    }
    if (state == S_RepeatRateEnter)
    { // === S_RepeatRateEnter ====================
        if (direction == 0)
        {
            nextState = S_0;
        }
        else if (direction != prev_direction)
        {
            nextState = S_RepeatDelayEnter;
        }
        else if ((direction == prev_direction))
        {
            time_begin_rate = time;
            nextState = S_RepeatRateWait;
        }
    }
    if (state == S_RepeatRateWait)
    { // === S_RepeatRateWait ====================
        if (direction == 0)
        {
            nextState = S_0;
        }
        else if (direction != prev_direction)
        {
            nextState = S_RepeatDelayEnter;
        }
        else if ((direction == prev_direction) && (time < (time_begin_rate + repeatRate)))
        {
            nextState = S_RepeatRateWait;
        }
        else if ((direction == prev_direction) && (time >= (time_begin_rate + repeatRate)))
        {
            nextState = S_RepeatRateEnter;
        }
    }
    prev_direction = direction;
    return nextState;
} // end nextState

void KeyboardStateAutomata::enterState(State nextState)
{
    state = nextState;
    if (state == S_RepeatDelayEnter)
    {
        //sendDirectionKey(prev_direction);
        if (send_direction_callback != NULL)
            (*send_direction_callback)(prev_direction);
        DBG_print("### emission dir:");
        DBG_println(prev_direction);
    }
    if (state == S_RepeatRateEnter)
    {
        //sendDirectionKey(prev_direction);
        if (send_direction_callback != NULL)
            (*send_direction_callback)(prev_direction);
        DBG_print("### emission (repeat) dir:");
        DBG_println(prev_direction);
    }
} // end enterSTate

// declare this in main file
// KeyboardStateAutomata automate;