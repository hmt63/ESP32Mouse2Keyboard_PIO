

// === DIRECTION ARRAOW DEFINITIONS AND METHODS ===============================================


#define NO_DIRECTION 0
#define DIRECTION_UP 1
#define DIRECTION_DOWN 2
#define DIRECTION_LEFT 3
#define DIRECTION_RIGHT 4

    /* defined in tinyusb/src/class/hid.hid.h
    #define HID_KEY_ARROW_RIGHT               0x4F
    #define HID_KEY_ARROW_LEFT                0x50
    #define HID_KEY_ARROW_DOWN                0x51
    #define HID_KEY_ARROW_UP                  0x52
    */

    enum State {
        S_0,
        S_RepeatDelayEnter,
        S_RepeatDelayWait,
        S_RepeatRateEnter,
        S_RepeatRateWait
    };

class KeyboardStateAutomata
{
public:
    unsigned int repeatDelay = 600; // repeat delay (like in windows 11) typica lly between 250ms and 1000ms
    unsigned int repeatRate = 300;  // repeat rate, typically between 2,5 / sec and 30 / sec, ie between 30ms and 500ms
    void (* send_direction_callback)(int) ;

private:
    State state;
    int prev_direction = 0; // prev_direction keeps the previous direction : it can be seen as a stack of size 1, in a stack automata.
    unsigned int time_begin_delay = 0;
    unsigned int time_begin_rate = 0;

    //void  (*keyboard_callback)(int) ;

    public : KeyboardStateAutomata();

public:
    void doAction(int direction, unsigned long time);

private:
    State nextState(int direction, unsigned long time);

private:
    void enterState(State nextState);

public:
    static void test(int testSequence[], int sequenceLen, int time_step);
}; // end class
